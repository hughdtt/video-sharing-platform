#! /usr/bin/env node

console.log('This script populates the S3 files to the database. Specified database as argument - e.g.: populatedb mongodb+srv://cooluser:coolpassword@cluster0-mbdj7.mongodb.net/local_library?retryWrites=true');

// Get arguments passed on command line
var userArgs = process.argv.slice(2);
/*
if (!userArgs[0].startsWith('mongodb')) {
    console.log('ERROR: You need to specify a valid mongodb URL as the first argument');
    return
}
*/

var async = require('async');
var Schedule = require('./models/schedule');

var mongoose = require('mongoose');
var mongoDB = userArgs[0];
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var schedules = [];

function scheduleCreate(id, day, timeslot,capacity,name,subject,cb) {
    scheduledetail = { 
      id:id,
      day:day,
      timeslot:timeslot,
      capacity:capacity, 
      user_list: {name, subject},
    }
      
    var schedule = new Schedule(scheduledetail);    
    schedule.save(function (err) {
      if (err) {
        cb(err, null)
        return
      }
      console.log('New file: ' + schedule);
      schedules.push(schedule)
      cb(null, schedule)
    });
}


function createSchedule(cb) {
    async.parallel([
        function (callback) {
            scheduleCreate('tue-fri-11111111', 'Tue-Fri', '6.30PM', 1 ,'Hugh', 'Math',
            callback)
        },
        ],
        cb);
}
async.series([
    //s3.addKeytoList(),
    createSchedule
],
// Optional callback
function(err, results) {
    if (err) {
        console.log('FINAL ERR: '+err);
    }
    else {
        console.log('FileInstances: '+ schedules);
        
    }
    // All done, disconnect from database
    mongoose.connection.close();
});

//tweak