require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var mongoose = require('mongoose');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var helmet = require('helmet');
var compression = require('compression');

//Routers
var auth = require ('./lib/auth');
var indexRouter = require('./routes/index');
var resourcesRouter = require('./routes/resources');
var dashboardRouter = require('./routes/dashboard');
var enrolmentRouter = require('./routes/enrolment');
var usersRouter = require('./routes/users');
var newsRouter = require('./routes/news');
var s3router = require('./routes/s3.router');

// Authentication Packages
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('./models/User');
var flash = require('express-flash');
var MongoStore = require('connect-mongo')(session);

// Configure the local strategy for use by Passport.
passport.use(
  new LocalStrategy(function(username, password, callback) {
    User.findOne({ username: username }, function(err, user) {
      if (err) {
        return callback(err);
      }
      if (!user) {
        return callback(null, false, { message: 'Incorrect username. ' });
      }
      if (!user.validatePassword(password)) {
        return callback(null, false, { message: 'Incorrect password.' });
      }
      return callback(null, user);
    });
  })
);

// Configure Passport authenticated session persistence.
passport.serializeUser(function(user, callback) {
  callback(null, user._id);
});

passport.deserializeUser(function(id, callback) {
  User.findById(id, function(err, user) {
    if (err) {
      return callback(err);
    }
    callback(null, user);
  });
});


var app = express();
app.use(helmet());

// Set up mongoose connection
var db = process.env.MONGO_URI;
mongoose
  .connect(
    db,
    {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false
    }
  )
  .then(() => console.log("MongoDB successfully connected"))
  .catch(err => console.log(err, "MongoDB unable to connect. Check IP configuration"));


// View engine setup
app.set('views', [path.join(__dirname, 'views'), path.join(__dirname, 'views/resources'), path.join(__dirname, 'views/user'), path.join(__dirname, 'views/dashboard'), path.join(__dirname, 'views/enrolment'), path.join(__dirname, 'views/posts')]);
app.set('view engine', 'pug');

// Express Setup

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(compression()); //Compress all routes
app.use(express.static(path.join(__dirname, 'public')));

// Authentication related middleware.
app.use(flash());
app.use(
  session({
    secret: process.env.SECRET_KEY,
    resave: false,
    saveUninitialized: true,
    store: new MongoStore({
      url: db,
      ttl: 7 * 24 * 60 * 60 // 7 days. 14 Default.
    })
    // cookie: { secure: true }
  })
);

// Initialize Passport and restore authentication state, if any,
// from the session.
app.use(passport.initialize());
app.use(passport.session());

// Pass isAuthenticated and current_user to all views.
app.use(function(req, res, next) {
  res.locals.isAuthenticated = req.isAuthenticated();
  // Delete salt and hash fields from req.user object before passing it.
  var safeUser = req.user;
  if (safeUser) {
    delete safeUser._doc.salt;
    delete safeUser._doc.hash;
  }
  res.locals.current_user = safeUser;
  next();
});

// Use our Authentication and Authorization middleware.
app.use(auth);

app.use('/', indexRouter);
app.use('/resources', resourcesRouter);
app.use('/users', usersRouter);
app.use('/dashboard', dashboardRouter);
app.use('/enrolment', enrolmentRouter);
app.use('/news', newsRouter);


//Should be unaccessible
app.use('/api', s3router);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;
