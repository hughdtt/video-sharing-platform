var { check,validationResult } = require('express-validator');
var async = require('async');

var Post = require('../models/Post');

exports.index = function(req, res, next) {
    async.parallel({
        post: function(callback) {
            Post.find({date: {$gte: 2020}}).exec(callback);
        },
        }, function(err, results) {
            if (err) { return next(err); }
            if (results.post==null) { // No results.
                var err = new Error('post not found');
                err.status = 404;
                return next(err);
            }
            res.render('post_index', { title: 'News List', post: results.post});
        });
};

exports.article_get = function(req, res, next) {
    async.parallel({
        post: function(callback) {
            Post.find({_id:req.params.id}).exec(callback);
        },
        }, function(err, results) {
            if (err) { return next(err); }
            if (results.post==null) { // No results.
                var err = new Error('post not found');
                err.status = 404;
                return next(err);
            }
            res.render('post_id', { title: 'Post ID', post: results.post});
        });
};

exports.article_post = function(req, res, next) {
    async.parallel({
        post: function(callback) {
            Post.find({_id:req.params.id}).exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        // Success
        else {
            async function deletePost(){
                const post = { _id: req.params.id};
                try {
                    await Post.deleteOne(post);
                    await res.redirect('/news');
                  } catch (error) {
                    console.error("error in delete post()", error);
                  }
            }
            deletePost();
        }
    });
};

exports.create_get = [
    // Continue processing.
    function (req, res, next) {
        res.render('post_create', {
            title: 'Post Create'
        });
    }
];
  
// Handle register on POST.
exports.create_post = [
    // Validate fields.
    check('title', 'Title must be at least 3 characters long.').isLength({ min: 3 }).trim(),
    check('body', 'Post must be at least 3 characters long.').isLength({ min: 3 }).trim(),

    // Process request after validation and sanitization.
    (req, res, next) => {
        // Extract the validation errors from a request.
        var errors = validationResult(req);

        // Get a handle on errors.array() array,
        // so we can push our own error messages into it.
        var errorsArray = errors.array();

        console.log(req.body, ': body msg');

        // Create a user object with escaped and trimmed data.
        var post = new Post({
            title: req.body.title,
            body: req.body.body,
            date: Date.now(),
            author: req.user,
        });

        if (errorsArray.length > 0) {
            // There are errors. Render the form again with sanitized values/error messages.
            res.render('post_create', {
                title: 'Create post',
                errors: errorsArray,
            });
            return;
        } else {
            post.save(err => {
                if (err) {
                    return next(err);
                }
                // User saved. Redirect to login page.
                req.flash('success', 'Successfully posted. You can go check it out now!');
                res.redirect('/news');
            });
        }
    }
];


exports.edit_get = function(req, res, next) {
    async.parallel({
        post: function(callback) {
            Post.find({_id:req.params.id}).exec(callback);
        },
        }, function(err, results) {
            if (err) { return next(err); }
            else {
                res.render('post_edit', { title: 'Edit', post: results.post});
            }
        });
};

exports.edit_post = [
    //Validate
    check('title', 'Title must not be empty').not().isEmpty().trim(),
    check('body', 'Body must not be empty').not().isEmpty().trim(),

    (req,res,next) => {
        var errors = validationResult(req);
        var errorsArray = errors.array();
        if (errorsArray.length > 0) {
            // There are errors. Render the form again with sanitized values/error messages.
            async.parallel({
                post: function(callback) {
                    Post.find({_id:req.params.id}).exec(callback);
                },
                }, function(err, results) {
                    if (err) { return next(err); }
                    else {
                        res.render('post_edit', { title: 'Edit', post: results.post, errors: errorsArray});
                        console.log(errorsArray);
                    }
                });
            return;
        } else {
            async function update() {
                const filter = { _id: req.params.id };
                const update = {
                    $set: {
                        title: req.body.title,
                        body: req.body.body,
                        date: Date.now(),
                    }
                };
                try {
                    await Post.findByIdAndUpdate(filter, update, { new: true });
                    req.flash('success', 'Successfully posted. You can go check it out now!');
                    res.redirect('/news');
                } catch (error) {
                    console.error("Error in Update()", error);
                }
            }
            update();
        }
    }
]