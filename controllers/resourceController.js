var File = require('../models/File');
var async = require('async');
var passport = require('passport');
var debug = require('debug')('resource');

//HOME Page - Home page i stuck here because lazy
exports.index = function(req, res) {
    async.parallel({
        File: function(callback) {
            File.find({}, 'title body date author').exec(callback);
        },
    }, function(err, results) {
        res.render('start', { error: err, data: results.File });
    });
    
};

exports.success = function(req, res) {
    res.render('success', { title: 'Success!'});
    
};

exports.home = function(req, res){
    async.parallel({
        File: function(callback) {
            File.find({}, 'title body date author').exec(callback);
        },
    }, function(err, results) {
        res.render('home', { error: err, data: results.File });
    });
}

//Resource Page - Display Home and Dyanmic Data
exports.resources = function(req, res) {
    res.render('../resources/resources', { title: 'Resources'});
};



// Display list of all Authors.
// exports.file_audio_list = function(req, res) {
//     File.find({}, 'subject type level page_number linkurl')
//     .exec(function (err, list_files) {
//       if (err) {return next(err)} 
//       else {
//             // Successful, so render
//             res.render('audio', { title: 'Audio List', file_audio_list: list_files});
//             return;
//             //console.log(list_files);
//         }
//     });
// };

// ENGLISH
// Display english resource primary page
exports.resource_english = function(req, res) {
    res.render('english', { title: 'English'});
};

// Display english audio subpage
exports.resource_english_audio = function(req, res){
    File.find({level:req.params.id, type:'Audio'})
        .exec(function (err, list_files) {
          if (err) {
            debug('update error:' + err)  
            return next(err)} 
          else {
                // Successful, so render
                list_files.sort();
                res.render('english_audio', { title: 'Reading and Listening Files', file_audio_list: list_files});
                return;
                //console.log(list_files);
            }
        });
};

// Display english audio subpage
exports.resource_english_answers = function(req, res){
    File.find({level:req.params.id, type:'Answer'})
        .exec(function (err, list_files) {
          if (err) {
            debug('update error:' + err)  
            return next(err)} 
          else {
                // Successful, so render
                list_files.sort();
                res.render('english_answer', { title: 'Answer', file_list: list_files});
                return;
                //console.log(list_files);
            }
        });
};

//-----------------------------------------------------------------------------------------

// MATH
// Display math resource primary page
exports.resource_math = function(req, res) {
    res.render('math', { title: 'Math'});
};

// Display math answer solution subpage
exports.resource_math_answer = function(req, res){
    File.find({level:req.params.id})
        .exec(function (err, list_files) {
          if (err) {
            console.log(req.params.id)
            debug('update error:' + err)  
            return next(err)} 
          else {
                console.log(list_files)
                res.render('math_answer', { title: 'Answer', file_list: list_files});
                return;
                //console.log(list_files);
            }
        });
};