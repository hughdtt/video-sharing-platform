const s3 = require('s3');
require('dotenv').config();
 
exports.doDownload = (req, res) => {
  var client = s3.createClient({
    s3Options: {
      accessKeyId: process.env.AWS_ACCESS_KEY,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
      region: process.env.AWS_REGION
    }
  });

  const downloadParams = {
    Bucket: process.env.AWS_BUCKET,
    Key: req.params.filetype + '/' + req.params.level + '/' + req.params.filename,
    ResponseExpires: 123456789,
  };
  
  //New Promise Structure for getting object, streaming and rendersing res ; currently optimization issue - calls the getObjects twice before rendering (bad)
  var downloadStream = client.downloadStream(downloadParams);

  downloadStream.on('error', function() {
    res.status(404).send('Not Found');
  });
  downloadStream.on('httpHeaders', function(statusCode, headers, resp) {
    // Set Headers
    res.set({
      'Content-Type': 'audio/mp3'
    });
    console.log(statusCode, headers, resp);
  });

  // Pipe download stream to response
  downloadStream.pipe(res);

}



