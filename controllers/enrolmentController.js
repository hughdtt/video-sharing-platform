//Requires
var Schedule = require('../models/schedule');

const {check,validationResult } = require('express-validator');
var async = require('async');

// Enrolments
// Display enrolment main page
exports.enrolment_primary = function(req, res) {
    res.render('enrolment', { title: 'Enrolment Forms'});
};

// Display scheduling main
exports.enrolment_scheduling = function(req, res){
    async.series({
        schedule: function(callback) {
            Schedule.find({}, 'timeslot capacity id day').exec(callback);
        },
        }, function(err, results) {
            if (err) { return next(err); }
            if (results.schedule==null) { // No results.
                var err = new Error('Schedule not found');
                err.status = 404;
                return next(err);
            }
            else {
                async function divide(){
                const mtArray = [];
                const tfArray = [];
                for (i=0; i<results.schedule.length; i++){
                    if (results.schedule[i].day === 'Mon-Thurs'){
                        mtArray.push(results.schedule[i]);
                    }
                    else {
                        tfArray.push(results.schedule[i]);
                    }
                }
                await mtArray.sort();
                await tfArray.sort();
                if (req.isAuthenticated()){
                    await res.render('scheduling', { title: 'Schedule', schedule_one: mtArray, schedule_two: tfArray});
                }
                else {
                    await res.render('scheduling', { title: 'Schedule', schedule_one: mtArray, schedule_two: tfArray});
                }
            }
            divide();
            }
        });
    
};

// Display scheduling forms
exports.enrolment_scheduling_form_get = function(req, res, next){
    async.parallel({
        schedule: function(callback) {
            Schedule.find({id:req.params.id}).exec(callback);
        },
        }, function(err, results) {
            if (err) { return next(err); }
            if (results.schedule==null) { // No results.
                var err = new Error('Schedule not found');
                err.status = 404;
                return next(err);
            }
            res.render('scheduling_form', { title: 'Fill out the form', schedule: results.schedule});
        });
};

// Display scheduling forms
exports.enrolment_scheduling_form_post = [

    // Validate fields.
    //check('name').isAlpha()
    //check('subject').isAlpha().withMessage('Subject must only be Math/ English/ Both').isLength({ max: 7 }).withMessage('Remember to choose Math/ English/ Both'),
    check('name').trim().escape().not().isEmpty().matches(/^[a-zA-Z0-9 _.-]*$/i).withMessage('Name must be in the only First Name and Initial of Last Name eg. Michael L'),
    check('subject').exists().isIn(['Math','English','Both']).withMessage('Subject must be Math, English or Both'),


    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);
        console.log(errors);

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/errors messages.
            async.parallel({
                schedule: function(callback) {
                    Schedule.find({id:req.params.id}).exec(callback);
                },
                }, function(err, results) {
                    if (err) { return next(err); }
                    if (results.schedule==null) { // No results.
                        var err = new Error('Schedule not found');
                        err.status = 404;
                        return next(err);
                    }
                    res.render('scheduling_form', { title: 'Fill out the form', schedule: results.schedule, errors: errors.array()});
                    return;
                });
        }
        else {
            // Data from form is valid.
            //Update User model and then update the corresponding schedule.
            async function update(){
                const filter = { id: req.params.id};
                //if subject == both then need to update next time slot aswell
                const adjacent = { id: req.params.id + 1}

                //Pushing the new user into user_list in the Schedule timeslot
                const update = {$push: {user_list: {
                    name: req.body.name,
                    subject: req.body.subject,
                }}};

                const adjUpdate = {$push: {user_list: {
                    name: req.body.name,
                    subject: req.body.subject + " (Second Half)",
                }}};

                try{
                    if (req.body.subject == 'Both'){

                        //Current Schedule Timeslot
                        await Schedule.findOneAndUpdate(filter, update, { new: true});
                        await Schedule.findOneAndUpdate(filter, {$inc : {'capacity' : 1}} , { new: true});
    
                        //Adjacent Schedule Timeslot
                        await Schedule.findOneAndUpdate(adjacent, adjUpdate, { new: true});
                        await Schedule.findOneAndUpdate(adjacent, {$inc : {'capacity' : 1}} , { new: true});
                        await res.render('success', { title: 'Success', name: req.body.name, subject: req.body.subject, timeslot: req.params.id});
                    }
                    else {
    
                        //Current Schedule Timeslot
                        await Schedule.findOneAndUpdate(filter, update, { new: true});
                        await Schedule.findOneAndUpdate(filter, {$inc : {'capacity' : 1}} , { new: true});
                        await res.render('success', { title: 'Success', name: req.body.name, subject: req.body.subject, timeslot: req.params.id});
                    }
                } catch (error) {
                    console.error("Error in Update()", error);
                  }
            }
            update();   
        }
    }
];

// Display scheduling list
exports.enrolment_scheduling_list_get = function(req, res, next){
    async.parallel({
        schedule: function(callback) {
            Schedule.find({id:req.params.id}).exec(callback);
        },
        }, function(err, results) {
            if (err) { return next(err); }
            else {
                res.render('userlist', { title: 'Viewing List', schedule: results.schedule[0].user_list, schedule_name: results.schedule});
            }
        });
};

// Display scheduling list
exports.enrolment_scheduling_list_post = function(req, res, next) {
    async.parallel({
        schedule: function(callback) {
            Schedule.find({id:req.params.id}).exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        // Success
        else {
            async function deleteUser(){
                const schedule = { id: req.params.id};
                const filter = results.schedule[0].user_list[req.body.id].id;
                const update = {$pull: { user_list:{_id: filter}}};
                try {
                    await Schedule.findOneAndUpdate(schedule, update, { new: true});
                    await Schedule.findOneAndUpdate(schedule, {$inc : {'capacity' : -1}} , { new: true});
                    await res.redirect('..');
                  } catch (error) {
                    console.error("error in delete user()", error);
                  }
            }
            deleteUser();
        }
    });
};

//tweak