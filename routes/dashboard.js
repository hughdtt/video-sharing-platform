var express = require('express');
var router = express.Router();

// Require controller modules.
var dashboard_controller = require('../controllers/dashboardController');

/// FILE ROUTES ///

// GET dashboard main page.
router.get('/', dashboard_controller.dashboard_primary);


//Exports
module.exports = router;