var express = require('express');
var router = express.Router();

// Require controller modules.
var news_controller = require('../controllers/newsController');

// GET home page.
router.get('/', news_controller.index);

router.get('/post/create', news_controller.create_get);

router.post('/post/create', news_controller.create_post);

router.get('/post/:id/edit', news_controller.edit_get);

router.post('/post/:id/edit', news_controller.edit_post);

router.get('/post/:id', news_controller.article_get);

router.post('/post/:id', news_controller.article_post);


module.exports = router;