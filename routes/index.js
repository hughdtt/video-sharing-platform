var express = require('express');
var router = express.Router();

// Require controller modules.
var resource_controller = require('../controllers/resourceController');

// GET home page.
router.get('/', resource_controller.index);

router.get('/home', resource_controller.home);

router.get('/success', resource_controller.success);

module.exports = router;
