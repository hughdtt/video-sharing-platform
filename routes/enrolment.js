var express = require('express');
var router = express.Router();

// Require controller modules.
var enrolment_controller = require('../controllers/enrolmentController');

/// FILE ROUTES ///

// GET enrolment main page.
router.get('/', enrolment_controller.enrolment_primary);

// GET scheduling main page.
router.get('/scheduling', enrolment_controller.enrolment_scheduling);


// GET scheduling form page.
router.get('/scheduling/form/:id', enrolment_controller.enrolment_scheduling_form_get);

// POST scheduling form page.
router.post('/scheduling/form/:id', enrolment_controller.enrolment_scheduling_form_post);

// GET scheduling form page.
router.get('/scheduling/list/:id', enrolment_controller.enrolment_scheduling_list_get);

// POST scheduling user delete page.
router.post('/scheduling/list/:id/', enrolment_controller.enrolment_scheduling_list_post);

//Exports
module.exports = router;