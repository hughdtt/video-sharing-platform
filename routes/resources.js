var express = require('express');
var router = express.Router();

// Require controller modules.
var resource_controller = require('../controllers/resourceController');

/// FILE ROUTES ///

// GET resources page.
router.get('/', resource_controller.resources);

// --------------------------------------------------------------------------
/// ENGLISH ///

// GET request english resource primary page.
router.get('/english',resource_controller.resource_english);

// GET request english audio subpage.
router.get('/english/listening/:id',resource_controller.resource_english_audio);

// GET request english answer subpage.
router.get('/english/answers/:id',resource_controller.resource_english_answers);

// --------------------------------------------------------------------------

/// MATH ///

// GET request math resource primary page
router.get('/math', resource_controller.resource_math);

// GET request math resource primary page
router.get('/math/answers/:id', resource_controller.resource_math_answer);

//Exports
module.exports = router;
