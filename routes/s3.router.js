let express = require('express');
let router = express.Router();
 
const awsWorker = require('../controllers/s3.controller.js');
 
router.get('/s3/files/:filetype/:level/:filename', awsWorker.doDownload);
 
module.exports = router;