var express = require('express');
var router = express.Router();

// Define allowed operations for roles.
// 0: User
// 1: Editor
// 2: Admin
var roles = {
  0: ['read'],
  1: ['read', 'create', 'update'],
  2: ['read', 'create', 'update', 'delete']
};

router.use(/^\//, [
  function(req, res, next) {
    // Use hard-coded operation.
    req.requested_operation = 'read';
    next();
  },
  confirmAuthentication,
  confirmRole
]);

router.use(/^\/api/, [
  function(req, res, next) {
    // Use hard-coded operation.
    req.requested_operation = 'read';
    next();
  },
  confirmAuthentication,
  confirmRole
]);

router.use(/^\/resources/, [
  function(req, res, next) {
    // Use hard-coded operation.
    req.requested_operation = 'read';
    next();
  },
  confirmAuthentication,
  confirmRole
]);

router.use(/^\/enrolment/, [
  function(req, res, next) {
    // Use hard-coded operation.
    req.requested_operation = 'read';
    next();
  },
  confirmAuthentication,
  confirmRole
]);

router.use(/^\/resources\/([a-zA-Z0-9]{1,})/, [
  function(req, res, next) {
    // Use hard-coded operation.
    req.requested_operation = 'read';
    next();
  },
  confirmAuthentication,
  confirmRole
]);

router.use(/^\/enrolment\/([a-zA-Z0-9]{1,})/, [
  function(req, res, next) {
    // Use hard-coded operation.
    req.requested_operation = 'read';
    next();
  },
  confirmAuthentication,
  confirmRole
]);

router.use(/^\/users\/register/, [
  function(req, res, next) {
    // Use hard-coded operation.
    req.requested_operation = 'create';
    next();
  },
  confirmAuthentication,
  confirmRole
]);

// Confirms that the user is authenticated.
function confirmAuthentication(req, res, next) {
  if (req.isAuthenticated()) {
    // Authenticated. Proceed to next function in the middleware chain.
    return next();
  } else {
    // Not authenticated. Redirect to login page and flash a message.
    req.flash('error', 'You need to login first!');
    res.redirect('/users/login');
  }
}

// Confirms that the user has appropriate permission.
function confirmRole(req, res, next) {
  var userRole = Number.parseInt(req.user.role);
  var operation = req.requested_operation;
  if (roles[userRole].includes(operation)) {
    // User has required permission.
    return next();
  } else {
    // User does not have required permission. Redirect.
    req.flash('error', "You're not authorized to access this page!");
    res.redirect('/users/stop');
  }
}

module.exports = router;