#! /usr/bin/env node

console.log('This script populates the S3 files to the database. Specified database as argument - e.g.: populatedb mongodb+srv://cooluser:coolpassword@cluster0-mbdj7.mongodb.net/local_library?retryWrites=true');

// Get arguments passed on command line
var userArgs = process.argv.slice(2);
/*
if (!userArgs[0].startsWith('mongodb')) {
    console.log('ERROR: You need to specify a valid mongodb URL as the first argument');
    return
}
*/

var async = require('async');
var File = require('./models/File');

var mongoose = require('mongoose');
var mongoDB = userArgs[0];
mongoose.connect(mongoDB, { useNewUrlParser: true,
  useUnifiedTopology: true  });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var files = [];

// var levelArray = ['001', '002-005', '006-007', '008-010', '011', '012-015', '016-017', '018-020', '021', '022-025', '026-027', '028-030', '031',
// '032-035', '036-037','038-040', '042-045', '046-047','048-050', '052-055','056', '057-060', '061',
// '066-067', '071','072-075', '076-077','082-085', '088-090', '091' ,'092-095', '097-100',
// '102-105', '106-107', '108-110', '111', '112-115', '116-117', '121', '126-127', '128-130', '131',
// '132-135', '138-140', '141', '146-147', '148-150', '151', '152-155', '156-157', '158-160','162-165', '166-167', '168-170',
// '191','192-195', '196', '197-200', 'FII Achievement Test'];

// const level = 'FII'

function fileCreate(subject, type, level, page_number,linkurl,cb) {
  filedetail = {
    subject: subject,
    type: type,
    level: level,
    page_number: page_number,
    linkurl: linkurl,
  }
  var file = new File(filedetail);    
    file.save(function (err) {
      if (err) {
        cb(err, null)
        return
      }
      console.log('New file: ' + file);
      files.push(file)
      cb(null, file)
    });
}

function createFile(cb) {
  async.parallel([
    function (callback) {
      fileCreate('English', 'Answer', '2A', '1-200', 'https://www.yumpu.com/en/embed/view/96wXvut2JDpyfHAo',
        callback)
    },
    function (callback) {
      fileCreate('English', 'Answer', 'AI', '1-200', 'https://www.yumpu.com/en/embed/view/CSEw5FaO1yQDfuih',
        callback)
    },
    function (callback) {
      fileCreate('English', 'Answer', 'AII', '1-200', 'https://www.yumpu.com/en/embed/view/3UCEi8Y7GoehfDtK',
        callback)
    },
    function (callback) {
      fileCreate('English', 'Answer', 'BI', '1-200', 'https://www.yumpu.com/en/embed/view/rMhL8Rt3VE4cBGFD',
        callback)
    },
    function (callback) {
      fileCreate('English', 'Answer', 'BII', '1-200', 'https://www.yumpu.com/en/embed/view/JnTAja0oB5zD4qmw',
        callback)
    },
    function (callback) {
      fileCreate('English', 'Answer', 'CI', '1-200', 'https://www.yumpu.com/en/embed/view/qP57j8srRFwXQHcf',
        callback)
    },
    function (callback) {
      fileCreate('English', 'Answer', 'CII', '1-200', 'https://www.yumpu.com/en/embed/view/J0szPBwONFjpQG2Z',
        callback)
    },
    function (callback) {
      fileCreate('English', 'Answer', 'DI', '1-200', 'https://www.yumpu.com/en/embed/view/ynKSuZJEwfMV2ipP',
        callback)
    },
    function (callback) {
      fileCreate('English', 'Answer', 'DII', '1-200', 'https://www.yumpu.com/en/embed/view/D5ESZFGJc8XV2OHs',
        callback)
    },
    function (callback) {
      fileCreate('English', 'Answer', 'EI', '1-200', 'https://www.yumpu.com/en/embed/view/vybtFiEoj2xSIXrU',
        callback)
    },
    function (callback) {
      fileCreate('English', 'Answer', 'EII', '1-200', 'https://www.yumpu.com/en/embed/view/Qhi7omN2kVslcewP',
        callback)
    },
    function (callback) {
      fileCreate('English', 'Answer', 'FI', '1-200', 'https://www.yumpu.com/en/embed/view/StPvJEbFZ0yTWKOk',
        callback)
    },
    function (callback) {
      fileCreate('English', 'Answer', 'FII', '1-200', 'https://www.yumpu.com/en/embed/view/avJrQmTlUIGcS4gz',
        callback)
    },
  ],
    cb);
}
async.series([
  //s3.addKeytoList(),
  createFile
],
// Optional callback
function(err, results) {
  if (err) {
      console.log('FINAL ERR: '+err);
  }
  else {
      console.log('FileInstances: '+ files);
      
  }
  // All done, disconnect from database
  mongoose.connection.close();
});

// function wait_promise(ms) 
// 	{
// 		return new Promise((resolve, reject) => {
// 		setTimeout(() => {
// 		resolve(ms);
// 		}, ms )
// 		})
// 	}  

// async function processFile(array){
//   try{
//     for (var i=0; i<array.length; i++){
//       fileCreate('English', 'Audio', level, array[i],  '/api/s3/files/Audio/FII/' + array[i] +'.mp3')
//       await wait_promise(100);
//     };
//     await wait_promise(1000);
//     mongoose.connection.close();
//   } catch (error){
//     console.log(error);
//   }
// }

// processFile();

  
