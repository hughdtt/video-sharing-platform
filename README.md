# Kumon Forest Lake Library App
Pugjs, Express/ Nodejs, AWS S3

# Project klibrare

klibrare a web solution to the storing/ viewing of audio files for Kumon Forest Lake.
The solution should be scalable and eventually provide some admin functionality.

# Technologies

## Front-End
- [Pugjs]
- [Materialize]

## Back-End
- [Node.js](https://nodejs.org/en/download/) ^10.0.0
- [Express]
- [Amazon S3]

## Contributors
* **Hugh Duong-Tran-Tien** - *Developer* - [hughdtt](https://gitlab.com/hughdtt)

