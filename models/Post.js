var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PostSchema = new Schema(
    {
      title: {type: String},
      body: {type: String},
      date: {type: Date, default: Date.now},
      author: {type: Schema.Types.ObjectId, ref: 'User'}
    }
);

// Virtual for Post's URL
PostSchema
.virtual('url')
.get(function () {
  return '/news/posts' + this.id;
});

//Export model
module.exports = mongoose.model('Post', PostSchema);