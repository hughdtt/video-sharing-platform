var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var FileSchema = new Schema(
    {
        subject: { type: String, required: true },
        type: { type: String, required: true },
        level: { type: String, required: true },
        page_number: { type: String, required: true },
        linkurl: { type: String, required: true }
    }
);

// Virtual for File's URL
FileSchema
.virtual('url')
.get(function () {
  return '/resources/files' + this.id;
});

//Export model
module.exports = mongoose.model('File', FileSchema);