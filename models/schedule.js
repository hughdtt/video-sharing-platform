var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ScheduleSchema = new Schema(
    {
        id: {type: String},
        day: { type: String, required: true },
        timeslot: { type: String, required: true },
        capacity: { type: Number, required: true },
        user_list: [{
          name: String, 
          subject: String,
        }]
    }
);

// Virtual for File's URL
ScheduleSchema
.virtual('url')
.get(function () {
  return '/enrolment/schedule' + this.id;
});

//Export model
module.exports = mongoose.model('Schedule', ScheduleSchema);