document.addEventListener('DOMContentLoaded', function() {
    M.AutoInit();
  
    var options = {
        fullWidth: true,
        indicators: true
    };

    var elems = document.querySelector('.carousel.carousel-slider');
    console.log(elems)
    var instances = M.Carousel.init(elems, options);
  })